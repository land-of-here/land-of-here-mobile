import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:land_of_here_mobile/theme/style.dart';
import 'package:land_of_here_mobile/navigation/routes.dart';
import 'package:land_of_here_mobile/redux/app-state.dart';
import 'package:land_of_here_mobile/redux/navigation/middleware.dart';
import 'package:land_of_here_mobile/redux/reducer.dart';
import 'package:land_of_here_mobile/navigation/route-redux-widget.dart';

void main() => runApp(LandOfHere());

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class LandOfHere extends StatelessWidget {
  final store = Store<AppState>(appReducer,
      initialState: AppState.loading(),
      middleware: createNavigationMiddleware());

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        navigatorKey: navigatorKey,
        navigatorObservers: [routeObserver],
        title: 'Land of Here',
        theme: appTheme(),
        onGenerateRoute: (RouteSettings settings) => generateRoutes(settings),
      ),
    );
  }
}
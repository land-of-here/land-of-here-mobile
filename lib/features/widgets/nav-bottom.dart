import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:land_of_here_mobile/navigation/constants.dart';
import 'package:land_of_here_mobile/redux/navigation/actions.dart';
import 'package:land_of_here_mobile/redux/app-state.dart';
import 'package:land_of_here_mobile/redux/selectors.dart';

class NavBottom extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: StoreConnector<AppState, _ViewModel>(
        distinct: true,
        converter: (store) => _ViewModel.fromStore(store),
        builder: (context, vm) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _getNavBottomItem(
              context,
              icon: Icon(Icons.near_me),
              routeName: NavigationConstants.Here,
              vm: vm
            ),
            _getNavBottomItem(
              context,
              icon: Icon(Icons.map),
              routeName: NavigationConstants.Explore,
              vm: vm
            ),
            _getNavBottomItem(
              context,
              icon: Icon(Icons.shopping_basket),
              routeName: NavigationConstants.Provide,
              vm: vm
            ),
            _getNavBottomItem(
              context,
              icon: Icon(Icons.stars),
              routeName: NavigationConstants.Explore,
              vm: vm
            ),
          ],
        ),
      ),
    );
  }

  Widget _addPadding(Widget child) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8),
    child: child,
  );

  Widget _getNavBottomItem(BuildContext context, 
    {Icon icon, String routeName, @required _ViewModel vm}) {
    if (!vm.route.contains(routeName))
      return _addPadding(
        IconButton(
          icon: icon,
          onPressed: () => vm.navigate(routeName),
          color: Colors.grey[400]
        ));
    else
      return _addPadding(IconButton(
        icon: icon,
        onPressed: () => vm.navigate(routeName),
        color: Theme.of(context).accentColor.withOpacity(0.7)
      ));
    }
}

class _ViewModel {
  final List<String> route;
  final Function(String) navigate;

  _ViewModel({@required this.route, @required this.navigate});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      route: currentRoute(store.state),
      navigate: (routeName) =>
        store.dispatch(NavigateReplaceAction(routeName))
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is _ViewModel &&
              runtimeType == other.runtimeType &&
              route == other.route;

  @override
  int get hashCode => route.hashCode;
}
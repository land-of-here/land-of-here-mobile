# Land of Here Mobile

The mobile app for Land of Here. 

## Development

This project is developed using Flutter, so if you haven't already, [install](https://flutter.dev/docs/get-started/install)!

Current dev uses [VS Code](https://code.visualstudio.com/download) for IDE so get the dev enviroment [setup](https://flutter.dev/docs/get-started/editor?tab=vscode).

Be sure to run `flutter doctor` and install mobile sdks/tools as needed.

### Quickstart
Commands given for Mac; just swap CMD > CTRL, OPT > ALT for PC.

1. `[CMD]+[shift][P]` (or from menu, View -> Command Palette)
2. Start typing "Flutter" and commands will be available. Choose `Select Device` > start iOS Simulater
3. Press `[F5]` or Debug> Start Debugging
   - will launch ios simulator. Be patience, takes a minute to build and transfer into simulator.
4. App should be running. Start developing!
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class NavTop extends StatelessWidget implements PreferredSizeWidget {
  final double height;

  const NavTop({
    Key key,
    @required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          color: Colors.grey[300],
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: Container(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: Text(
                    'Land of Here',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Theme.of(context).accentColor
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.menu),
                  color: Colors.grey[600],
                  iconSize: 32,
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
              ]),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}

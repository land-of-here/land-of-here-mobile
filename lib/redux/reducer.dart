import 'package:land_of_here_mobile/redux/app-state.dart';
import 'package:land_of_here_mobile/redux/loading/reducer.dart';
import 'package:land_of_here_mobile/redux/navigation/reducer.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    isLoading: loadingReducer(state.isLoading, action),
    route: navigationReducer(state.route, action)
  );
}
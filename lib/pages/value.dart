import 'package:flutter/material.dart';
import 'package:land_of_here_mobile/features/widgets/page-menu-wrapper.dart';

class Value extends StatelessWidget {

  Widget _getBody(double width, double height) => 
    Container(
        width: width,
        height: height,
        child: Stack(
          children: <Widget>[
            Center(
              child: Text('Value')
            ) 
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return PageMenuWrapper(_getBody(width, height));
  }
}
import 'package:land_of_here_mobile/redux/app-state.dart';

List<String> currentRoute(AppState state) => state.route;
import 'package:flushbar/flushbar_route.dart';
import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';

import 'package:land_of_here_mobile/pages/here.dart';
import 'package:land_of_here_mobile/pages/explore.dart';
import 'package:land_of_here_mobile/pages/provide.dart';
import 'package:land_of_here_mobile/pages/value.dart';

import 'package:land_of_here_mobile/navigation/route-redux-widget.dart';
import 'package:land_of_here_mobile/navigation/constants.dart';
import 'package:land_of_here_mobile/services/notifications.dart';

Route generateRoutes(RouteSettings settings) {
  switch (settings.name) {
    case NavigationConstants.Here:
      return MainRoute(Here(), settings: settings);
    case NavigationConstants.Explore:
      return MainRoute(Explore(), settings: settings);
    case NavigationConstants.Provide:
      return MainRoute(Provide(), settings: settings);
    case NavigationConstants.Value:
      return MainRoute(Value(), settings: settings);
    case NavigationConstants.NOTIFICATION_SUCCESS:
      return buildNotification(NotificationService.successFactory());
    case NavigationConstants.NOTIFICATION_ERROR:
      return buildNotification(NotificationService.errorFactory());
    default:
      return MainRoute(Here(), settings: settings);
  }
}

class MainRoute<T> extends MaterialPageRoute<T> {
  MainRoute(Widget widget, {RouteSettings settings})
      : super(
          builder: (_) => RouteAwareWidget(child: widget),
          settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return FadeTransition(opacity: animation, child: child);
  }
}

FlushbarRoute buildNotification(Flushbar type) {
  return FlushbarRoute(
    theme: ThemeData.light(),
    flushbar: type,
    settings: RouteSettings(name: FLUSHBAR_ROUTE_NAME),
  );
}
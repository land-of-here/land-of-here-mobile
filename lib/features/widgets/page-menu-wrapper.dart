import 'package:flutter/material.dart';
import 'package:land_of_here_mobile/features/widgets/nav-top.dart';
import 'package:land_of_here_mobile/features/widgets/nav-bottom.dart';

class PageMenuWrapper extends StatelessWidget {
  final Widget body;

  PageMenuWrapper(this.body);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NavTop(height: 150),
      bottomNavigationBar: NavBottom(),
      body: body,
    );
  }
}
class NavigationConstants {
  // Notification routes
  static const String NOTIFICATION_SUCCESS = "notification-success-route";
  static const String NOTIFICATION_ERROR = "notification-error-route";
  static const String NOTIFICATION_INFO = "notification-info-route";

  // Pages routes
  static const String Here = "/here";
  static const String Explore = "/explore";
  static const String Provide = "/provide";
  static const String Value = "/value";
}
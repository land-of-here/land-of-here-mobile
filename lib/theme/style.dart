import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    primaryColor: Colors.white,
    accentColor: Colors.lightGreen[500],
    hintColor: Colors.tealAccent,
    dividerColor: Colors.blueGrey,
    buttonColor: Colors.greenAccent,
    scaffoldBackgroundColor: Colors.white,
    canvasColor: Colors.black,
  );
}